-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 29, 2021 at 10:59 AM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clinica_reclamos`
--

-- --------------------------------------------------------

--
-- Table structure for table `causas`
--

CREATE TABLE `causas` (
  `id` smallint UNSIGNED NOT NULL,
  `derecho_id` smallint UNSIGNED DEFAULT NULL,
  `codigo` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `definicion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `defini` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `causas`
--

INSERT INTO `causas` (`id`, `derecho_id`, `codigo`, `nombre`, `definicion`, `defini`) VALUES
(1, 5, '1101', 'Emitir recetas farmacologicas sin la denominación genérica internacional, datos erroneos, o incompleta.', 'Se considera aquellos reclamos relacionados a la entrega de recetas emitidas por el profesional de salud, sin consignar el nombre genérico del medicamento, con letra ilegible, incompleta, entre otras.', 'Receta con medicamentos de marca y/o incompleta y/o ilegible.'),
(2, 5, '1103', 'Direccionar  al usuario a comprar medicamentos o dispositivos médicos fuera del establecimiento de salud.', 'Inducir al usuario a comprar determinados medicamentos o dispositivos médicos fuera de la IPRESS a pesar de estar cubiertos a contar con stock en el establecimiento.', 'Me dicen que compre afuera.'),
(3, 5, '1104', 'Direccionar ai usuario a realizarse procedimientos médicos a quirurgicos fuera del establecimiento de salud.', 'Inducir al usuario a realizarse procedimientos médicos o quirúrgicos fuera del establecimiento, pese a estar cubiertos o disponibles en la IPRESS.', 'Me dijeron que me realice el procedimiento afuera.'),
(7, 3, '003', 'demox', 'demox', 'demox'),
(8, 4, '0015', 'nueva11', 'nueva12', 'nueva13');

-- --------------------------------------------------------

--
-- Table structure for table `conclusiones_anticipadas`
--

CREATE TABLE `conclusiones_anticipadas` (
  `id` tinyint UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `conclusiones_anticipadas`
--

INSERT INTO `conclusiones_anticipadas` (`id`, `nombre`) VALUES
(3, 'CONCILIACIÓN'),
(1, 'DESISTIMIENTO POR ESCRITO'),
(5, 'LAUDO ARBITRAL'),
(4, 'TRANSACCIÓN EXTRAJUDICIAL'),
(2, 'TRATO DIRECTO');

-- --------------------------------------------------------

--
-- Table structure for table `derechos`
--

CREATE TABLE `derechos` (
  `id` smallint UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `derechos`
--

INSERT INTO `derechos` (`id`, `nombre`) VALUES
(3, 'Acceso a la Información'),
(1, 'Acceso a los Servicios de Salud'),
(4, 'Atención y Recuperación de la Salud'),
(2, 'Consentimiento Informado'),
(6, 'Otros'),
(5, 'Protección de Derechos');

-- --------------------------------------------------------

--
-- Table structure for table `envios_resultados`
--

CREATE TABLE `envios_resultados` (
  `id` tinyint UNSIGNED NOT NULL,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `envios_resultados`
--

INSERT INTO `envios_resultados` (`id`, `nombre`) VALUES
(2, 'CORREO ELECTRÓNICO'),
(1, 'DOMICILIO CONSIGNADO EN EL LIBRO DE RECLAMACIONES EN SALUD'),
(3, 'OTRA DIRECCIÓN PROPORCIONADA POR EL USUARIO O TERCERO LEGITIMADO A EFECTOS DE SER NOTIFICADO');

-- --------------------------------------------------------

--
-- Table structure for table `estados`
--

CREATE TABLE `estados` (
  `id` smallint UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `estados`
--

INSERT INTO `estados` (`id`, `nombre`) VALUES
(8, 'ACUMULADO'),
(4, 'ARCHIVADO POR DUPLICIDAD'),
(9, 'CONCLUIDO'),
(2, 'EN TRAMITE'),
(10, 'REGISTRADO'),
(1, 'RESUELTO'),
(3, 'TRASLADADO');

-- --------------------------------------------------------

--
-- Table structure for table `etapas`
--

CREATE TABLE `etapas` (
  `id` tinyint UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `etapas`
--

INSERT INTO `etapas` (`id`, `nombre`) VALUES
(1, 'Admisión y Registro'),
(4, 'Archivo y Custodia del Expediente'),
(2, 'Evaluación e Investigación'),
(3, 'Resultado y Notificación');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `IAFAS`
--

CREATE TABLE `IAFAS` (
  `id` tinyint UNSIGNED NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `IAFAS`
--

INSERT INTO `IAFAS` (`id`, `nombre`) VALUES
(9, 'Convenio IPAZA'),
(8, 'Convenio Universidad Agraria'),
(10, 'Convenio VEGAZA'),
(6, 'Mapfre EPS'),
(7, 'Mapfre Seguros y Reaseguros'),
(1, 'Pacifico EPS'),
(2, 'Pacifico Seguros'),
(11, 'Petróleos del Peru'),
(12, 'Protecta'),
(3, 'Rimac EPS'),
(4, 'Rimac Seguros y Reaseguros'),
(5, 'Sanitas del Peru');

-- --------------------------------------------------------

--
-- Table structure for table `medios_recepcion`
--

CREATE TABLE `medios_recepcion` (
  `id` tinyint UNSIGNED NOT NULL,
  `medio` varchar(200) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `medios_recepcion`
--

INSERT INTO `medios_recepcion` (`id`, `medio`) VALUES
(5, 'Documento escrito'),
(2, 'Libro de Reclamaciones Físico'),
(1, 'Libro de Reclamaciones Virtual'),
(3, 'Llamada telefónica'),
(7, 'Reclamo coparticipado con otra administrada'),
(4, 'Reclamo presencial'),
(6, 'Reclamo trasladado de otra administrada');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_06_17_231027_create_estados_table', 1),
(5, '2021_06_17_231716_create_tipos_table', 1),
(6, '2021_06_17_232201_create_causas_table', 1),
(7, '2021_06_18_003254_create_origenes_table', 1),
(8, '2021_06_18_003333_create_reclamos_table', 1),
(9, '2021_07_13_155110_create_table_derechos', 2);

-- --------------------------------------------------------

--
-- Table structure for table `origenes`
--

CREATE TABLE `origenes` (
  `id` smallint UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `padre_id` smallint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `origenes`
--

INSERT INTO `origenes` (`id`, `nombre`, `padre_id`) VALUES
(1, 'Administrativa', NULL),
(2, 'Asistencial', NULL),
(3, 'Atención al usuario', NULL),
(4, 'Contabilidad', 1),
(5, 'RR.HH', 1),
(6, 'Finanzas', 1),
(7, 'Logística', 1),
(8, 'Facturación', 1),
(9, 'Calidad', 1),
(10, 'Auditoría', 1),
(11, 'Limpieza y Mantenimiento', 1),
(12, 'Hospitalización', 2),
(13, 'Emergencia', 2),
(14, 'Farmacia', 2),
(15, 'Rayos x', 2),
(16, 'Ecografía', 2),
(17, 'Densitometría', 2),
(18, 'Consultorio Externo', 2),
(19, 'Laboratorio', 2),
(20, 'Nutrición', 2),
(21, 'Centro Quirúgico', 2),
(22, 'Sala Procedimientos', 2),
(23, 'Sala de Recuperación', 2),
(24, 'Unidad Cuidados Intensivos', 2),
(25, 'Admisión Ambulatorio', 3),
(26, 'Admisión Emergencia', 3),
(27, 'Admisión Odontología', 3),
(28, 'Central Telefónica', 3),
(29, 'Sanidad', 3),
(30, 'Plataforma de Atención al usuario', 3),
(31, 'Óptica', 3),
(32, 'Admisión Laboratorio', 3),
(33, 'Admisión Resocentro', 3),
(34, 'Presupuestos', 3),
(35, 'Mesa de partes', 3),
(44, 'demo-area2', NULL),
(49, 'origen2.2.', 44);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reclamos`
--

CREATE TABLE `reclamos` (
  `id` bigint UNSIGNED NOT NULL,
  `tipo_id` smallint UNSIGNED DEFAULT NULL,
  `causa_id` smallint UNSIGNED DEFAULT NULL,
  `origen_id` smallint UNSIGNED DEFAULT NULL,
  `estado_id` smallint UNSIGNED DEFAULT '10',
  `traslado_id` tinyint UNSIGNED DEFAULT NULL,
  `traslado_codigo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `user2_id` int UNSIGNED DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `analisis` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `medidas_adoptadas` text COLLATE utf8mb4_unicode_ci,
  `conclusion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `suceso_at` date DEFAULT NULL,
  `send_mail` tinyint(1) NOT NULL DEFAULT '1',
  `resuelto_at` date DEFAULT NULL,
  `dias_max_resp` tinyint UNSIGNED NOT NULL DEFAULT '30',
  `medio_recepcion_id` tinyint UNSIGNED DEFAULT NULL,
  `hoja_nro` char(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `administrado_tp` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'IPRESS',
  `codigo_renipress` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '00012975',
  `recibido_at` date DEFAULT NULL,
  `paciente_tp` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iafas_id` tinyint UNSIGNED DEFAULT NULL,
  `etapa_id` tinyint UNSIGNED DEFAULT NULL,
  `resultado_id` tinyint UNSIGNED DEFAULT NULL,
  `notificacion_id` tinyint UNSIGNED DEFAULT NULL,
  `notificacion_at` date DEFAULT NULL,
  `conclusiona_id` tinyint UNSIGNED DEFAULT NULL COMMENT 'Conclusion anticipada',
  `delete_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reclamos`
--

INSERT INTO `reclamos` (`id`, `tipo_id`, `causa_id`, `origen_id`, `estado_id`, `traslado_id`, `traslado_codigo`, `user_id`, `user2_id`, `descripcion`, `analisis`, `medidas_adoptadas`, `conclusion`, `created_at`, `updated_at`, `suceso_at`, `send_mail`, `resuelto_at`, `dias_max_resp`, `medio_recepcion_id`, `hoja_nro`, `administrado_tp`, `codigo_renipress`, `recibido_at`, `paciente_tp`, `iafas_id`, `etapa_id`, `resultado_id`, `notificacion_id`, `notificacion_at`, `conclusiona_id`, `delete_at`) VALUES
(37, 11, 3, 32, 8, 5, 'CODTRASx', 9, NULL, 'demo', 'demo de analisisxxxxx', 'xxxxxx', 'demo de conclusionesxxxxx', '2021-09-12 05:00:00', '2021-09-25 20:29:02', '2021-09-01', 1, '2021-09-01', 30, 2, '0001', 'IPRESS', '00012975', '2021-09-01', 'ASEGURADO', 1, 2, 1, 1, '2021-09-01', 2, NULL),
(38, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, 'demo2', NULL, NULL, NULL, '2021-09-12 05:00:00', '2021-09-22 21:35:07', '2021-09-10', 1, NULL, 30, 3, '0002', 'IPRESS', '00012975', '2021-09-12', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, 'demox', NULL, NULL, NULL, '2021-09-13 05:00:00', '2021-09-13 20:01:01', '2021-09-09', 1, NULL, 30, 2, '0004', 'IPRESS', '00012975', '2021-09-13', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 9, 7, 25, 1, 3, NULL, 8, NULL, 'aaaaaaaaa', NULL, NULL, NULL, '2021-09-13 05:00:00', '2021-09-23 21:32:44', '2021-09-04', 1, NULL, 30, 5, '0003', 'IPRESS', '00012975', '2021-09-13', 'PARTICULAR', 9, 1, 6, 2, NULL, 3, NULL),
(41, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, 'aaaaaaaaa', NULL, NULL, NULL, '2021-09-13 20:42:33', '2021-09-22 21:34:57', '2021-09-04', 1, NULL, 30, 5, '0003', 'IPRESS', '00012975', '2021-09-05', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reclamo_docs`
--

CREATE TABLE `reclamo_docs` (
  `id` int UNSIGNED NOT NULL,
  `reclamo_id` int UNSIGNED NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre_cliente` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci COMMENT='documentos de los reclamos';

--
-- Dumping data for table `reclamo_docs`
--

INSERT INTO `reclamo_docs` (`id`, `reclamo_id`, `nombre`, `nombre_cliente`, `created_at`, `updated_at`, `usuario`) VALUES
(2, 37, '6153b5f6ceeb5.pdf', 'seleccion del canon pali.pdf', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(3, 37, '6153b5f72fa1b.pdf', '37-practices_spanish.pdf', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `resultados`
--

CREATE TABLE `resultados` (
  `id` tinyint UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `resultados`
--

INSERT INTO `resultados` (`id`, `nombre`) VALUES
(6, 'CONCLUIDO ANTICIPADAMENTE'),
(2, 'FUNDADO'),
(3, 'FUNDADO PARCIAL'),
(5, 'IMPROCEDENTE'),
(4, 'INFUNDADO'),
(1, 'PENDIENTE');

-- --------------------------------------------------------

--
-- Table structure for table `tipos`
--

CREATE TABLE `tipos` (
  `id` smallint UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`) VALUES
(9, 'Atención a domicilio, consulta ambulatoria'),
(10, 'Atención a domicilio, urgencia o emergencia'),
(5, 'Centro Obstétrico'),
(4, 'Centro Quirúrgico'),
(1, 'Consulta Externa'),
(3, 'Emergencia'),
(7, 'Farmacia'),
(2, 'Hospitalización'),
(12, 'Infraestructura'),
(11, 'Oficinas o áreas administrativas de IAFAS o IPRESS o UGIPRESS'),
(13, 'Referencia y Contrareferencia'),
(8, 'Servicios Médicos de Apoyo'),
(6, 'UCI o UCIN');

-- --------------------------------------------------------

--
-- Table structure for table `traslados`
--

CREATE TABLE `traslados` (
  `id` tinyint UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `traslados`
--

INSERT INTO `traslados` (`id`, `nombre`) VALUES
(3, 'IAFAS'),
(1, 'IPRESS'),
(4, 'RENIPRESS'),
(5, 'RIAFAS'),
(2, 'UGIPRESS');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `rol` enum('Admin','Guest') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Admin',
  `dip_tp` enum('DNI','RUC','PASAPORTE','CDE','DIE','CUI','CNV','PTP','OTROS') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `domicilio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `codigo_historia` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `rol`, `dip_tp`, `dip`, `password`, `name`, `domicilio`, `email`, `email_verified_at`, `telefono`, `created_at`, `updated_at`, `codigo_historia`) VALUES
(1, 'Admin', 'DNI', '1', 'e10adc3949ba59abbe56e057f20f883e', 'Admin', 'Peru', 'admin@admin.com', NULL, NULL, NULL, NULL, NULL),
(2, 'Guest', 'DNI', '0912229000', '6605d6e367980d370dba729867658867', 'carlos otirralde', 'dmiicilio111', 'correo@gmail.com', NULL, '1232223', '2021-06-21 19:25:52', '2021-07-23 07:41:13', 'H-2021009-A'),
(3, 'Guest', 'RUC', 'RUC1', '8a08ab4cdc90a8efdb9295807c267afa', 'EMPRESA1', 'EMPRESA1-DOMICILIO FISCAL', 'EMPRESA1@GMAIL.COM', NULL, 'EMPRESA1-TLF', '2021-06-22 01:17:10', '2021-06-22 01:17:10', NULL),
(4, 'Guest', 'DNI', '0912290076', '6eba1d751e78d66a0524448205db0e2c', 'christian perez', 'lima-peru', 'crhistian@gmail.com', NULL, 'nomelose', '2021-06-22 06:19:54', '2021-06-22 06:19:54', NULL),
(5, 'Guest', 'DNI', 'dip2', 'b78b7910e1d341ac45dc923212c46f24', 'nombre2', 'dir2', 'email2', NULL, 'tlf2', '2021-06-22 19:36:48', '2021-06-22 19:36:48', NULL),
(6, 'Guest', 'DNI', 'dni1', '402e2e531b408c3ac3e27b55cdf96b45', 'DNI1', 'DNI1', 'DNI1', NULL, 'DNI1', '2021-06-23 02:20:57', '2021-06-23 02:20:57', NULL),
(7, 'Guest', 'DNI', 'REPRE1', 'cf0529454e37dcdb9594e824ef86b45e', 'REPRE1', 'REPRE1', 'REPRE1', NULL, 'REPRE1', '2021-06-23 02:20:57', '2021-06-23 02:20:57', NULL),
(8, 'Guest', 'DNI', '07129394', '82db353378ede13c383a4cc4388179bb', 'SILVA CARDENAS, BLANCA JANEE', 'saasfsaf', 'asdafsdf', NULL, 'sdasafad', '2021-06-24 02:31:33', '2021-06-24 02:31:33', NULL),
(9, 'Guest', 'RUC', '20607342483', '8bac0f2ee6a63a715d0c2c234e742106', 'BRIALESOFT SOLUCIONES INTEGRALES S.A.C.', 'AV. PERIMETRICA NRO. 806, LIMA-LIMA-CARABAYLLO', 's@gmail.com', NULL, '56576', '2021-06-27 20:49:59', '2021-09-21 19:48:46', 'H-0001'),
(10, 'Guest', 'PASAPORTE', '0912290087', 'c46750058b4fdcb3c12f4e4018036a4d', 'sssss', 'adsdsd', 'qwe@gmail.com', NULL, 'AASSAS', '2021-07-09 21:35:30', '2021-07-09 21:35:30', NULL),
(11, 'Guest', 'PASAPORTE', '121212', '93279e3308bdbbeed946fc965017f67a', 'dddff', 'asdsds', 'sws@gmail.com', NULL, 'sdsdd', '2021-07-09 21:35:30', '2021-07-09 21:35:30', NULL),
(12, 'Guest', 'OTROS', '0000', '4a7d1ed414474e4033ac29ccb8653d9b', 'demo de usr sin ci', 'gye', 'c', NULL, '123', '2021-07-18 08:42:25', '2021-07-18 08:42:25', NULL),
(13, 'Guest', 'OTROS', 'wwwe', 'f1a1e88ce9c1ffda9d92e148a4c046af', 'ada', 'sadfasd', NULL, NULL, 'asfasdfs', '2021-07-20 08:00:22', '2021-07-20 08:00:22', NULL),
(14, 'Guest', 'OTROS', '01-M-05', '9a02cdcec6089af3cf98b72caff6dc63', 'usuarios demo', '9 de octubre', NULL, NULL, '34555-3333', '2021-07-21 07:50:19', '2021-07-21 07:50:19', NULL),
(15, 'Guest', 'OTROS', '00001', '4c68cea7e58591b579fd074bcdaff740', 'Usuario sin nombre', 'no tiene domicilio', NULL, NULL, 'no tiene tlf', '2021-07-23 07:44:50', '2021-07-23 07:50:05', 'H-20210001'),
(16, 'Admin', NULL, NULL, '$2y$10$o9f8IqaUyujkpc0IomM.buw2d913aabwBDrwjN824Us8Ct3VLHto2', 'carlos', NULL, 'itu@gmail.com', NULL, NULL, '2021-10-29 19:45:08', '2021-10-29 19:45:08', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_count_causas`
-- (See below for the actual view)
--
CREATE TABLE `v_count_causas` (
`cantidad` bigint
,`codigo` varchar(18)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_count_estados`
-- (See below for the actual view)
--
CREATE TABLE `v_count_estados` (
`cantidad` bigint
,`nombre` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_count_origenes`
-- (See below for the actual view)
--
CREATE TABLE `v_count_origenes` (
`cantidad` bigint
,`nombre` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_count_tipos`
-- (See below for the actual view)
--
CREATE TABLE `v_count_tipos` (
`cantidad` bigint
,`nombre` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `v_count_causas`
--
DROP TABLE IF EXISTS `v_count_causas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_count_causas`  AS SELECT count(0) AS `cantidad`, ifnull(`b`.`codigo`,'SIN CAUSA DEFINIDA') AS `codigo` FROM (`reclamos` `a` left join `causas` `b` on((`a`.`causa_id` = `b`.`id`))) GROUP BY `a`.`causa_id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_count_estados`
--
DROP TABLE IF EXISTS `v_count_estados`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_count_estados`  AS SELECT count(0) AS `cantidad`, ifnull(`b`.`nombre`,'SIN ESTADO DEFINIDO') AS `nombre` FROM (`reclamos` `a` left join `estados` `b` on((`a`.`estado_id` = `b`.`id`))) GROUP BY `a`.`estado_id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_count_origenes`
--
DROP TABLE IF EXISTS `v_count_origenes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_count_origenes`  AS SELECT count(0) AS `cantidad`, ifnull(`b`.`nombre`,'SIN ORIGEN DEFINO') AS `nombre` FROM (`reclamos` `a` left join `origenes` `b` on(((`a`.`origen_id` = `b`.`id`) and (`b`.`padre_id` is null)))) GROUP BY `a`.`origen_id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_count_tipos`
--
DROP TABLE IF EXISTS `v_count_tipos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_count_tipos`  AS SELECT count(0) AS `cantidad`, ifnull(`b`.`nombre`,'SIN TIPO DEFINIDO') AS `nombre` FROM (`reclamos` `a` left join `tipos` `b` on((`a`.`tipo_id` = `b`.`id`))) GROUP BY `a`.`tipo_id` ORDER BY `b`.`nombre` ASC ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `causas`
--
ALTER TABLE `causas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `causas_nombre_unique` (`nombre`),
  ADD KEY `derecho_id` (`derecho_id`);

--
-- Indexes for table `conclusiones_anticipadas`
--
ALTER TABLE `conclusiones_anticipadas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `derechos`
--
ALTER TABLE `derechos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `derechos_nombre_unique` (`nombre`);

--
-- Indexes for table `envios_resultados`
--
ALTER TABLE `envios_resultados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`) USING BTREE;

--
-- Indexes for table `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `etapas`
--
ALTER TABLE `etapas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `IAFAS`
--
ALTER TABLE `IAFAS`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `medios_recepcion`
--
ALTER TABLE `medios_recepcion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `medio` (`medio`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `origenes`
--
ALTER TABLE `origenes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `reclamos`
--
ALTER TABLE `reclamos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_id_foreign` (`tipo_id`),
  ADD KEY `causa_id_foreign` (`causa_id`),
  ADD KEY `origen_id_foreign` (`origen_id`),
  ADD KEY `estado_id_foreign` (`estado_id`),
  ADD KEY `user2_id_foreign` (`user2_id`),
  ADD KEY `user_id_foreign` (`user_id`) USING BTREE,
  ADD KEY `medio_recepcion_id_foreign` (`medio_recepcion_id`),
  ADD KEY `iafas_id_foreign` (`iafas_id`),
  ADD KEY `etapa_id_foreign` (`etapa_id`),
  ADD KEY `resultado_id_foreign` (`resultado_id`),
  ADD KEY `traslado_id_foreign` (`traslado_id`),
  ADD KEY `notificacion_id_foreign` (`notificacion_id`),
  ADD KEY `conclusiona_id_foreign` (`conclusiona_id`);

--
-- Indexes for table `reclamo_docs`
--
ALTER TABLE `reclamo_docs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `resultados`
--
ALTER TABLE `resultados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `traslados`
--
ALTER TABLE `traslados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `codido_historia` (`codigo_historia`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `causas`
--
ALTER TABLE `causas`
  MODIFY `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `conclusiones_anticipadas`
--
ALTER TABLE `conclusiones_anticipadas`
  MODIFY `id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `derechos`
--
ALTER TABLE `derechos`
  MODIFY `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `envios_resultados`
--
ALTER TABLE `envios_resultados`
  MODIFY `id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `estados`
--
ALTER TABLE `estados`
  MODIFY `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `etapas`
--
ALTER TABLE `etapas`
  MODIFY `id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `IAFAS`
--
ALTER TABLE `IAFAS`
  MODIFY `id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `medios_recepcion`
--
ALTER TABLE `medios_recepcion`
  MODIFY `id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `origenes`
--
ALTER TABLE `origenes`
  MODIFY `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `reclamos`
--
ALTER TABLE `reclamos`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `reclamo_docs`
--
ALTER TABLE `reclamo_docs`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `resultados`
--
ALTER TABLE `resultados`
  MODIFY `id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `traslados`
--
ALTER TABLE `traslados`
  MODIFY `id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `causas`
--
ALTER TABLE `causas`
  ADD CONSTRAINT `causas_ibfk_1` FOREIGN KEY (`derecho_id`) REFERENCES `derechos` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `reclamos`
--
ALTER TABLE `reclamos`
  ADD CONSTRAINT `reclamos_causa_id_foreign` FOREIGN KEY (`causa_id`) REFERENCES `causas` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_1` FOREIGN KEY (`medio_recepcion_id`) REFERENCES `medios_recepcion` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_2` FOREIGN KEY (`iafas_id`) REFERENCES `IAFAS` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_3` FOREIGN KEY (`etapa_id`) REFERENCES `etapas` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_4` FOREIGN KEY (`resultado_id`) REFERENCES `resultados` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_5` FOREIGN KEY (`traslado_id`) REFERENCES `traslados` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_6` FOREIGN KEY (`notificacion_id`) REFERENCES `envios_resultados` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_7` FOREIGN KEY (`conclusiona_id`) REFERENCES `conclusiones_anticipadas` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_origen_id_foreign` FOREIGN KEY (`origen_id`) REFERENCES `origenes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_tipo_id_foreign` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_user2_id_foreign` FOREIGN KEY (`user2_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_users_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
