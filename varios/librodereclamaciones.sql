-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 05-08-2022 a las 16:18:40
-- Versión del servidor: 5.7.33
-- Versión de PHP: 8.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `librodereclamaciones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `causas`
--

CREATE TABLE `causas` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `derecho_id` smallint(5) UNSIGNED DEFAULT NULL,
  `codigo` char(5) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `definicion` text COLLATE utf8_spanish_ci NOT NULL,
  `defini` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `causas`
--

INSERT INTO `causas` (`id`, `derecho_id`, `codigo`, `nombre`, `definicion`, `defini`) VALUES
(1, 5, '1101', 'Emitir recetas farmacologicas sin la denominación genérica internacional, datos erroneos, o incompleta.', 'Se considera aquellos reclamos relacionados a la entrega de recetas emitidas por el profesional de salud, sin consignar el nombre genérico del medicamento, con letra ilegible, incompleta, entre otras.', 'Receta con medicamentos de marca y/o incompleta y/o ilegible.'),
(2, 5, '1103', 'Direccionar  al usuario a comprar medicamentos o dispositivos médicos fuera del establecimiento de salud.', 'Inducir al usuario a comprar determinados medicamentos o dispositivos médicos fuera de la IPRESS a pesar de estar cubiertos a contar con stock en el establecimiento.', 'Me dicen que compre afuera.'),
(3, 5, '1104', 'Direccionar ai usuario a realizarse procedimientos médicos a quirurgicos fuera del establecimiento de salud.', 'Inducir al usuario a realizarse procedimientos médicos o quirúrgicos fuera del establecimiento, pese a estar cubiertos o disponibles en la IPRESS.', 'Me dijeron que me realice el procedimiento afuera.'),
(7, 3, '003', 'demox', 'demox', 'demox'),
(8, 4, '0015', 'nueva11', 'nueva12', 'nueva13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conclusiones_anticipadas`
--

CREATE TABLE `conclusiones_anticipadas` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `conclusiones_anticipadas`
--

INSERT INTO `conclusiones_anticipadas` (`id`, `nombre`) VALUES
(3, 'CONCILIACIÓN'),
(1, 'DESISTIMIENTO POR ESCRITO'),
(5, 'LAUDO ARBITRAL'),
(4, 'TRANSACCIÓN EXTRAJUDICIAL'),
(2, 'TRATO DIRECTO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `derechos`
--

CREATE TABLE `derechos` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `derechos`
--

INSERT INTO `derechos` (`id`, `nombre`) VALUES
(3, 'Acceso a la Información'),
(1, 'Acceso a los Servicios de Salud'),
(4, 'Atención y Recuperación de la Salud'),
(2, 'Consentimiento Informado'),
(6, 'Otros'),
(5, 'Protección de Derechos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envios_resultados`
--

CREATE TABLE `envios_resultados` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `envios_resultados`
--

INSERT INTO `envios_resultados` (`id`, `nombre`) VALUES
(2, 'CORREO ELECTRÓNICO'),
(1, 'DOMICILIO CONSIGNADO EN EL LIBRO DE RECLAMACIONES EN SALUD'),
(3, 'OTRA DIRECCIÓN PROPORCIONADA POR EL USUARIO O TERCERO LEGITIMADO A EFECTOS DE SER NOTIFICADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `nombre`) VALUES
(8, 'ACUMULADO'),
(4, 'ARCHIVADO POR DUPLICIDAD'),
(9, 'CONCLUIDO'),
(2, 'EN TRAMITE'),
(10, 'REGISTRADO'),
(1, 'RESUELTO'),
(3, 'TRASLADADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etapas`
--

CREATE TABLE `etapas` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `etapas`
--

INSERT INTO `etapas` (`id`, `nombre`) VALUES
(1, 'Admisión y Registro'),
(4, 'Archivo y Custodia del Expediente'),
(2, 'Evaluación e Investigación'),
(3, 'Resultado y Notificación');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `connection` text COLLATE utf8_spanish_ci NOT NULL,
  `queue` text COLLATE utf8_spanish_ci NOT NULL,
  `payload` longtext COLLATE utf8_spanish_ci NOT NULL,
  `exception` longtext COLLATE utf8_spanish_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `iafas`
--

CREATE TABLE `iafas` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `iafas`
--

INSERT INTO `iafas` (`id`, `nombre`) VALUES
(9, 'Convenio IPAZA'),
(8, 'Convenio Universidad Agraria'),
(10, 'Convenio VEGAZA'),
(6, 'Mapfre EPS'),
(7, 'Mapfre Seguros y Reaseguros'),
(1, 'Pacifico EPS'),
(2, 'Pacifico Seguros'),
(11, 'Petróleos del Peru'),
(12, 'Protecta'),
(3, 'Rimac EPS'),
(4, 'Rimac Seguros y Reaseguros'),
(5, 'Sanitas del Peru');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medios_recepcion`
--

CREATE TABLE `medios_recepcion` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `medio` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `medios_recepcion`
--

INSERT INTO `medios_recepcion` (`id`, `medio`) VALUES
(5, 'Documento escrito'),
(2, 'Libro de Reclamaciones Físico'),
(1, 'Libro de Reclamaciones Virtual'),
(3, 'Llamada telefónica'),
(7, 'Reclamo coparticipado con otra administrada'),
(4, 'Reclamo presencial'),
(6, 'Reclamo trasladado de otra administrada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_06_17_231027_create_estados_table', 1),
(5, '2021_06_17_231716_create_tipos_table', 1),
(6, '2021_06_17_232201_create_causas_table', 1),
(7, '2021_06_18_003254_create_origenes_table', 1),
(8, '2021_06_18_003333_create_reclamos_table', 1),
(9, '2021_07_13_155110_create_table_derechos', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `origenes`
--

CREATE TABLE `origenes` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `padre_id` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `origenes`
--

INSERT INTO `origenes` (`id`, `nombre`, `padre_id`) VALUES
(1, 'Administrativa', NULL),
(2, 'Asistencial', NULL),
(3, 'Atención al usuario', NULL),
(4, 'Contabilidad', 1),
(5, 'RR.HH', 1),
(6, 'Finanzas', 1),
(7, 'Logística', 1),
(8, 'Facturación', 1),
(9, 'Calidad', 1),
(10, 'Auditoría', 1),
(11, 'Limpieza y Mantenimiento', 1),
(12, 'Hospitalización', 2),
(13, 'Emergencia', 2),
(14, 'Farmacia', 2),
(15, 'Rayos x', 2),
(16, 'Ecografía', 2),
(17, 'Densitometría', 2),
(18, 'Consultorio Externo', 2),
(19, 'Laboratorio', 2),
(20, 'Nutrición', 2),
(21, 'Centro Quirúgico', 2),
(22, 'Sala Procedimientos', 2),
(23, 'Sala de Recuperación', 2),
(24, 'Unidad Cuidados Intensivos', 2),
(25, 'Admisión Ambulatorio', 3),
(26, 'Admisión Emergencia', 3),
(27, 'Admisión Odontología', 3),
(28, 'Central Telefónica', 3),
(29, 'Sanidad', 3),
(30, 'Plataforma de Atención al usuario', 3),
(31, 'Óptica', 3),
(32, 'Admisión Laboratorio', 3),
(33, 'Admisión Resocentro', 3),
(34, 'Presupuestos', 3),
(35, 'Mesa de partes', 3),
(44, 'demo-area2', NULL),
(49, 'origen2.2.', 44);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reclamos`
--

CREATE TABLE `reclamos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_id` smallint(5) UNSIGNED DEFAULT NULL,
  `causa_id` smallint(5) UNSIGNED DEFAULT NULL,
  `origen_id` smallint(5) UNSIGNED DEFAULT NULL,
  `estado_id` smallint(5) UNSIGNED DEFAULT '10',
  `traslado_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `traslado_codigo` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user2_id` int(10) UNSIGNED DEFAULT NULL,
  `descripcion` text COLLATE utf8_spanish_ci,
  `analisis` text COLLATE utf8_spanish_ci,
  `medidas_adoptadas` text COLLATE utf8_spanish_ci,
  `conclusion` text COLLATE utf8_spanish_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `suceso_at` date DEFAULT NULL,
  `send_mail` tinyint(1) NOT NULL DEFAULT '1',
  `resuelto_at` date DEFAULT NULL,
  `dias_max_resp` tinyint(3) UNSIGNED NOT NULL DEFAULT '30',
  `medio_recepcion_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `hoja_nro` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `administrado_tp` varchar(20) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'IPRESS',
  `codigo_renipress` varchar(10) COLLATE utf8_spanish_ci NOT NULL DEFAULT '00012975',
  `recibido_at` date DEFAULT NULL,
  `paciente_tp` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `iafas_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `etapa_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `resultado_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `notificacion_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `notificacion_at` date DEFAULT NULL,
  `conclusiona_id` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Conclusion anticipada',
  `delete_at` datetime DEFAULT NULL,
  `ma_estado` enum('ACTIVO','CULMINADO') COLLATE utf8_spanish_ci DEFAULT 'ACTIVO',
  `ma_inicio` date DEFAULT NULL,
  `ma_fin` date DEFAULT NULL,
  `ma_tipo` smallint(5) UNSIGNED DEFAULT NULL,
  `ma_proceso` smallint(5) UNSIGNED DEFAULT NULL,
  `ma_proceso2` smallint(5) UNSIGNED DEFAULT NULL,
  `ma_procesoo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `observacionr` text COLLATE utf8_spanish_ci,
  `usrs_involucrados` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reclamo_docs`
--

CREATE TABLE `reclamo_docs` (
  `id` int(10) UNSIGNED NOT NULL,
  `reclamo_id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `nombre_cliente` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `usuario` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='documentos de los reclamos';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultados`
--

CREATE TABLE `resultados` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `resultados`
--

INSERT INTO `resultados` (`id`, `nombre`) VALUES
(6, 'CONCLUIDO ANTICIPADAMENTE'),
(2, 'FUNDADO'),
(3, 'FUNDADO PARCIAL'),
(5, 'IMPROCEDENTE'),
(4, 'INFUNDADO'),
(1, 'PENDIENTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE `tipos` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`) VALUES
(9, 'Atención a domicilio, consulta ambulatoria'),
(10, 'Atención a domicilio, urgencia o emergencia'),
(5, 'Centro Obstétrico'),
(4, 'Centro Quirúrgico'),
(1, 'Consulta Externa'),
(3, 'Emergencia'),
(7, 'Farmacia'),
(2, 'Hospitalización'),
(12, 'Infraestructura'),
(11, 'Oficinas o áreas administrativas de IAFAS o IPRESS o UGIPRESS'),
(13, 'Referencia y Contrareferencia'),
(8, 'Servicios Médicos de Apoyo'),
(6, 'UCI o UCIN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traslados`
--

CREATE TABLE `traslados` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `traslados`
--

INSERT INTO `traslados` (`id`, `nombre`) VALUES
(3, 'IAFAS'),
(1, 'IPRESS'),
(4, 'RENIPRESS'),
(5, 'RIAFAS'),
(2, 'UGIPRESS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `rol` enum('Admin','Guest') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Admin',
  `dip_tp` enum('DNI','RUC','PASAPORTE','CDE','DIE','CUI','CNV','PTP','OTROS') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `domicilio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `telefono` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `codigo_historia` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `rol`, `dip_tp`, `dip`, `password`, `name`, `domicilio`, `email`, `email_verified_at`, `telefono`, `created_at`, `updated_at`, `codigo_historia`) VALUES
(1, 'Admin', 'DNI', '1', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'Peru', 'admin@admin.com', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_count_causas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_count_causas` (
`cantidad` bigint(21)
,`codigo` varchar(18)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_count_estados`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_count_estados` (
`cantidad` bigint(21)
,`nombre` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_count_origenes`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_count_origenes` (
`cantidad` bigint(21)
,`nombre` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_count_tipos`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_count_tipos` (
`cantidad` bigint(21)
,`nombre` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_desistimiento`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_desistimiento` (
`id` bigint(20) unsigned
,`tipo_id` smallint(5) unsigned
,`causa_id` smallint(5) unsigned
,`origen_id` smallint(5) unsigned
,`estado_id` smallint(5) unsigned
,`traslado_id` tinyint(3) unsigned
,`traslado_codigo` varchar(10)
,`user_id` int(10) unsigned
,`user2_id` int(10) unsigned
,`descripcion` text
,`analisis` text
,`medidas_adoptadas` text
,`conclusion` text
,`created_at` timestamp
,`updated_at` timestamp
,`suceso_at` date
,`send_mail` tinyint(1)
,`resuelto_at` date
,`dias_max_resp` tinyint(3) unsigned
,`medio_recepcion_id` tinyint(3) unsigned
,`hoja_nro` varchar(10)
,`administrado_tp` varchar(20)
,`codigo_renipress` varchar(10)
,`recibido_at` date
,`paciente_tp` varchar(20)
,`iafas_id` tinyint(3) unsigned
,`etapa_id` tinyint(3) unsigned
,`resultado_id` tinyint(3) unsigned
,`notificacion_id` tinyint(3) unsigned
,`notificacion_at` date
,`conclusiona_id` tinyint(3) unsigned
,`delete_at` datetime
,`ma_estado` enum('ACTIVO','CULMINADO')
,`ma_inicio` date
,`ma_fin` date
,`ma_tipo` smallint(5) unsigned
,`ma_proceso` smallint(5) unsigned
,`ma_proceso2` smallint(5) unsigned
,`ma_procesoo` varchar(100)
,`observacionr` text
,`usrs_involucrados` text
,`name` varchar(255)
,`dip` varchar(20)
,`domicilio` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_tramas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_tramas` (
`id` bigint(20) unsigned
,`col_a` varchar(6)
,`col_b` int(1)
,`col_c` int(5)
,`col_d` int(5)
,`col_e` int(1)
,`col_f` varchar(10)
,`col_g` decimal(3,0)
,`col_h` varchar(16)
,`col_i` int(2)
,`col_j` varchar(20)
,`col_k` varchar(255)
,`col_l` varchar(255)
,`col_o` int(2)
,`col_p` varchar(20)
,`col_q` varchar(255)
,`col_r` varchar(255)
,`col_u` varchar(2)
,`col_v` varchar(255)
,`col_w` varchar(255)
,`col_x` varchar(255)
,`col_y` tinyint(3) unsigned
,`col_z` varchar(8)
,`col_aa` text
,`col_ab` int(2)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_count_causas`
--
DROP TABLE IF EXISTS `v_count_causas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_count_causas`  AS SELECT count(0) AS `cantidad`, ifnull(`b`.`codigo`,'SIN CAUSA DEFINIDA') AS `codigo` FROM (`reclamos` `a` left join `causas` `b` on((`a`.`causa_id` = `b`.`id`))) GROUP BY `a`.`causa_id``causa_id`  ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_count_estados`
--
DROP TABLE IF EXISTS `v_count_estados`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_count_estados`  AS SELECT count(0) AS `cantidad`, ifnull(`b`.`nombre`,'SIN ESTADO DEFINIDO') AS `nombre` FROM (`reclamos` `a` left join `estados` `b` on((`a`.`estado_id` = `b`.`id`))) GROUP BY `a`.`estado_id``estado_id`  ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_count_origenes`
--
DROP TABLE IF EXISTS `v_count_origenes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_count_origenes`  AS SELECT count(0) AS `cantidad`, ifnull(`b`.`nombre`,'SIN ORIGEN DEFINO') AS `nombre` FROM (`reclamos` `a` left join `origenes` `b` on(((`a`.`origen_id` = `b`.`id`) and isnull(`b`.`padre_id`)))) GROUP BY `a`.`origen_id``origen_id`  ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_count_tipos`
--
DROP TABLE IF EXISTS `v_count_tipos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_count_tipos`  AS SELECT count(0) AS `cantidad`, ifnull(`b`.`nombre`,'SIN TIPO DEFINIDO') AS `nombre` FROM (`reclamos` `a` left join `tipos` `b` on((`a`.`tipo_id` = `b`.`id`))) GROUP BY `a`.`tipo_id` ORDER BY `b`.`nombre` ASC  ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_desistimiento`
--
DROP TABLE IF EXISTS `v_desistimiento`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_desistimiento`  AS SELECT `a`.`id` AS `id`, `a`.`tipo_id` AS `tipo_id`, `a`.`causa_id` AS `causa_id`, `a`.`origen_id` AS `origen_id`, `a`.`estado_id` AS `estado_id`, `a`.`traslado_id` AS `traslado_id`, `a`.`traslado_codigo` AS `traslado_codigo`, `a`.`user_id` AS `user_id`, `a`.`user2_id` AS `user2_id`, `a`.`descripcion` AS `descripcion`, `a`.`analisis` AS `analisis`, `a`.`medidas_adoptadas` AS `medidas_adoptadas`, `a`.`conclusion` AS `conclusion`, `a`.`created_at` AS `created_at`, `a`.`updated_at` AS `updated_at`, `a`.`suceso_at` AS `suceso_at`, `a`.`send_mail` AS `send_mail`, `a`.`resuelto_at` AS `resuelto_at`, `a`.`dias_max_resp` AS `dias_max_resp`, `a`.`medio_recepcion_id` AS `medio_recepcion_id`, `a`.`hoja_nro` AS `hoja_nro`, `a`.`administrado_tp` AS `administrado_tp`, `a`.`codigo_renipress` AS `codigo_renipress`, `a`.`recibido_at` AS `recibido_at`, `a`.`paciente_tp` AS `paciente_tp`, `a`.`iafas_id` AS `iafas_id`, `a`.`etapa_id` AS `etapa_id`, `a`.`resultado_id` AS `resultado_id`, `a`.`notificacion_id` AS `notificacion_id`, `a`.`notificacion_at` AS `notificacion_at`, `a`.`conclusiona_id` AS `conclusiona_id`, `a`.`delete_at` AS `delete_at`, `a`.`ma_estado` AS `ma_estado`, `a`.`ma_inicio` AS `ma_inicio`, `a`.`ma_fin` AS `ma_fin`, `a`.`ma_tipo` AS `ma_tipo`, `a`.`ma_proceso` AS `ma_proceso`, `a`.`ma_proceso2` AS `ma_proceso2`, `a`.`ma_procesoo` AS `ma_procesoo`, `a`.`observacionr` AS `observacionr`, `a`.`usrs_involucrados` AS `usrs_involucrados`, `b`.`name` AS `name`, `b`.`dip` AS `dip`, `b`.`domicilio` AS `domicilio` FROM (`reclamos` `a` join `users` `b` on((`a`.`user_id` = `b`.`id`)))  ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_tramas`
--
DROP TABLE IF EXISTS `v_tramas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_tramas`  AS SELECT `a`.`id` AS `id`, date_format(`a`.`recibido_at`,'%Y%m') AS `col_a`, 1 AS `col_b`, 12975 AS `col_c`, 12975 AS `col_d`, (case `a`.`estado_id` when 3 then 3 else 1 end) AS `col_e`, (case `a`.`estado_id` when 3 then `a`.`traslado_codigo` else '12975' end) AS `col_f`, (case `a`.`medio_recepcion_id` when (1 or 3) then `a`.`medio_recepcion_id` else 2 end) AS `col_g`, concat('12975-',`a`.`hoja_nro`) AS `col_h`, (case when (`b`.`dip_tp` = 'DNI') then 1 when (`b`.`dip_tp` = 'CDE') then 2 when (`b`.`dip_tp` = 'PASAPORTE') then 3 when (`b`.`dip_tp` = 'DIE') then 4 when (`b`.`dip_tp` = 'CUI') then 5 when (`b`.`dip_tp` = 'CNV') then 6 when (`b`.`dip_tp` = 'PTP') then 10 when (`b`.`dip_tp` = 'RUC') then 11 else 12 end) AS `col_i`, `b`.`dip` AS `col_j`, (case when (`b`.`dip_tp` = 'RUC') then `b`.`name` end) AS `col_k`, (case when (`b`.`dip_tp` <> 'RUC') then `b`.`name` end) AS `col_l`, (case when (`c`.`dip_tp` = 'DNI') then 1 when (`c`.`dip_tp` = 'CDE') then 2 when (`c`.`dip_tp` = 'PASAPORTE') then 3 when (`c`.`dip_tp` = 'DIE') then 4 when (`c`.`dip_tp` = 'CUI') then 5 when (`c`.`dip_tp` = 'CNV') then 6 when (`c`.`dip_tp` = 'PTP') then 10 when (`c`.`dip_tp` = 'RUC') then 11 when (`c`.`dip_tp` = 'OTROS') then 12 else NULL end) AS `col_o`, `c`.`dip` AS `col_p`, (case when (`c`.`dip_tp` = 'RUC') then `c`.`name` else NULL end) AS `col_q`, (case when (`c`.`dip_tp` <> 'RUC') then `c`.`name` else NULL end) AS `col_r`, (case when `a`.`send_mail` then 'Si' else 'No' end) AS `col_u`, (case when `a`.`send_mail` then `b`.`email` else NULL end) AS `col_v`, `b`.`domicilio` AS `col_w`, `b`.`telefono` AS `col_x`, `a`.`medio_recepcion_id` AS `col_y`, date_format(`a`.`recibido_at`,'%Y%m%d') AS `col_z`, `a`.`descripcion` AS `col_aa`, (case when (`a`.`origen_id` = 18) then 1 when (`a`.`origen_id` = 12) then 2 when (`a`.`origen_id` = 13) then 3 when (`a`.`origen_id` = 21) then 4 when (`a`.`origen_id` = 0) then 5 when (`a`.`origen_id` = 24) then 6 when (`a`.`origen_id` = 14) then 7 when (`a`.`origen_id` = 0) then 8 when (`a`.`origen_id` = 25) then 9 when (`a`.`origen_id` = 0) then 10 when (`a`.`origen_id` = 0) then 11 when (`a`.`origen_id` = 0) then 12 when (`a`.`origen_id` = 0) then 13 else NULL end) AS `col_ab` FROM ((`reclamos` `a` join `users` `b` on((`a`.`user_id` = `b`.`id`))) left join `users` `c` on((`a`.`user2_id` = `c`.`id`))) ORDER BY `a`.`recibido_at` ASC  ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `causas`
--
ALTER TABLE `causas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `causas_nombre_unique` (`nombre`),
  ADD KEY `derecho_id` (`derecho_id`);

--
-- Indices de la tabla `conclusiones_anticipadas`
--
ALTER TABLE `conclusiones_anticipadas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `derechos`
--
ALTER TABLE `derechos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `derechos_nombre_unique` (`nombre`);

--
-- Indices de la tabla `envios_resultados`
--
ALTER TABLE `envios_resultados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`) USING BTREE;

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `etapas`
--
ALTER TABLE `etapas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `iafas`
--
ALTER TABLE `iafas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `medios_recepcion`
--
ALTER TABLE `medios_recepcion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `medio` (`medio`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `origenes`
--
ALTER TABLE `origenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_id_foreign` (`tipo_id`),
  ADD KEY `causa_id_foreign` (`causa_id`),
  ADD KEY `origen_id_foreign` (`origen_id`),
  ADD KEY `estado_id_foreign` (`estado_id`),
  ADD KEY `user2_id_foreign` (`user2_id`),
  ADD KEY `user_id_foreign` (`user_id`) USING BTREE,
  ADD KEY `medio_recepcion_id_foreign` (`medio_recepcion_id`),
  ADD KEY `iafas_id_foreign` (`iafas_id`),
  ADD KEY `etapa_id_foreign` (`etapa_id`),
  ADD KEY `resultado_id_foreign` (`resultado_id`),
  ADD KEY `traslado_id_foreign` (`traslado_id`),
  ADD KEY `notificacion_id_foreign` (`notificacion_id`),
  ADD KEY `conclusiona_id_foreign` (`conclusiona_id`);

--
-- Indices de la tabla `reclamo_docs`
--
ALTER TABLE `reclamo_docs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `resultados`
--
ALTER TABLE `resultados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `traslados`
--
ALTER TABLE `traslados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `codido_historia` (`codigo_historia`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `causas`
--
ALTER TABLE `causas`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `conclusiones_anticipadas`
--
ALTER TABLE `conclusiones_anticipadas`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `derechos`
--
ALTER TABLE `derechos`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `envios_resultados`
--
ALTER TABLE `envios_resultados`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `etapas`
--
ALTER TABLE `etapas`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `iafas`
--
ALTER TABLE `iafas`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `medios_recepcion`
--
ALTER TABLE `medios_recepcion`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `origenes`
--
ALTER TABLE `origenes`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reclamo_docs`
--
ALTER TABLE `reclamo_docs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resultados`
--
ALTER TABLE `resultados`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `traslados`
--
ALTER TABLE `traslados`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `causas`
--
ALTER TABLE `causas`
  ADD CONSTRAINT `causas_ibfk_1` FOREIGN KEY (`derecho_id`) REFERENCES `derechos` (`id`);

--
-- Filtros para la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD CONSTRAINT `reclamos_causa_id_foreign` FOREIGN KEY (`causa_id`) REFERENCES `causas` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_1` FOREIGN KEY (`medio_recepcion_id`) REFERENCES `medios_recepcion` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_2` FOREIGN KEY (`iafas_id`) REFERENCES `iafas` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_3` FOREIGN KEY (`etapa_id`) REFERENCES `etapas` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_4` FOREIGN KEY (`resultado_id`) REFERENCES `resultados` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_5` FOREIGN KEY (`traslado_id`) REFERENCES `traslados` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_6` FOREIGN KEY (`notificacion_id`) REFERENCES `envios_resultados` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_ibfk_7` FOREIGN KEY (`conclusiona_id`) REFERENCES `conclusiones_anticipadas` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_origen_id_foreign` FOREIGN KEY (`origen_id`) REFERENCES `origenes` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_tipo_id_foreign` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_user2_id_foreign` FOREIGN KEY (`user2_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reclamos_users_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
