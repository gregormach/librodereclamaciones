@extends('adminlte::page')

@section('title', 'Reclamos - Tramas')

@section('content_header')
    <h1 class="text-center">RECLAMOS -> Tramas</h1>
@endsection

@section('content')
<div class="row m-0 justify-content-center">
    <div class="col-3">
        <form action="{{ route('reclamos.tramas') }}" method="get">
            <div class="form-group">
                <label for="desde" class="h4">Desde el</label>
                <input type="date" name="desde" id="desde" class="form-control">
            </div>
            
            <div class="form-group">
                <label for="hasta" class="h4">Hasta el</label>
                <input type="date" name="hasta" id="hasta" class="form-control">
            </div>

            <button type="submit" class="btn btn-primary form-control">Generar</button>
        </form>
    </div>
</div>
@endsection

@section('js')
<script>
	$(document).ready(function () {
		// hoy
		let hoy = new Date();

	})
</script>
@endsection
